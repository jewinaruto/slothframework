![sloth_logo-03.png](https://bitbucket.org/repo/MaBodk/images/1090747978-sloth_logo-03.png)

The objective of SlothFramework is to take away as much boilerplate code as possible.
With this in mind it offers annotations to save the state of a field, or to inject views. But you can do more, check the features on the [wiki](https://bitbucket.org/jewinaruto/slothframework/wiki/Home).

It's in a very early state, please be patient.

# Importing the project in AndroidStudio

Slothframework is a library project. You can import it as an [aar file](http://tools.android.com/tech-docs/new-build-system/aar-format) or as code.

## Importing as an AAR file ##

First you have to download the AAR from the download section and copy it to your *libs* folder.

Then you have to add this to your build.graddle file:
```
repositories {
    flatDir {
        dirs 'libs'
    }
}

dependencies {
    compile (name:'slothframework', ext:'aar')
}
```

Sync the graddle project and it should be working.

You have two example projects to check out:

* [InjectView and SaveState example](https://bitbucket.org/jewinaruto/viewinjectionandsavestateexample)
* [ListView with custom header and parallax effect](https://bitbucket.org/jewinaruto/customheaderlistview)

## Importing project as code ###

You can import the project as code. You need to download/clone the code an link it to your project.
By now in AndroidStudio you can't link projects outside your own code.

For example, if you download the code to a folder called *libraries* inside your project you have to add this in your **build.graddle** file:

```
dependencies {
    compile project (':libraries:slothframework:slothframework')
}
``` 

And this in your **settings.graddle** file:

```
include ':libraries:slothframework:slothframework'
```

## Related post ##

* [Custom header in ListView with parallax effect](http://slothdevelopers.wordpress.com/2014/05/29/custom-header-with-parallax-effect-in-listview/)
* [Implementing view injection, slothframwork](http://slothdevelopers.wordpress.com/2014/05/27/implementing-view-injection-slothframework/)

## Contact

I'm trying to write (and update) the wiki as much as I can. I also would like to learn more about *Android testing* and add test to this project when I have more time.

I hope you like it.

If you have suggestions or want to *contact* me you can do it on: <jewinaruto@gmail.com>

You also can check my blog:
http://slothdevelopers.wordpress.com/