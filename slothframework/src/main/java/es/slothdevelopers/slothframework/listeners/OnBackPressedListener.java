package es.slothdevelopers.slothframework.listeners;

public interface OnBackPressedListener {
    public boolean onBackPressed();

}
