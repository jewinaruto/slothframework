package es.slothdevelopers.slothframework.application;

import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;
import android.util.Log;
import android.view.LayoutInflater;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import es.slothdevelopers.slothframework.SlothComponent;
import es.slothdevelopers.slothframework.annotations.Singleton;
import es.slothdevelopers.slothframework.utils.MessageUtils;
import es.slothdevelopers.slothframework.utils.SlothLog;

public class SlothApplication extends Application {
    private Map<String, Object> singletonMap = new HashMap<String, Object>();




    public void addSingletonField(Object object, Field field) throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        String singletonId = field.getAnnotation(Singleton.class).value();
        if (singletonMap.containsKey(singletonId)){
            restoreField(field, object, singletonId);
        } else {
            storeSingleton(object, field, singletonId);
        }
    }

    private void restoreField(Field field, Object object, String singletonId) {
        try {
            if ((singletonMap.get(singletonId) == null) && (field.get(object) != null)){
                storeSingleton(object, field, singletonId);
                return;
            }
            field.set(object, singletonMap.get(singletonId));
        } catch (IllegalArgumentException ex){
            SlothLog log = new SlothLog(object);
            log.error(MessageUtils.errorInjectingIncompatibleType(singletonMap.get(singletonId),field));
            throw ex;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    private void storeSingleton(Object object, Field field, String singletonId) throws IllegalAccessException {
        if (isBasicTypeOrSpecialType(field.getType())){
            SlothLog log = new SlothLog(object);
            log.warn(MessageUtils.adviceSingletonIsPrimitiveType(field));
        }
        Object obj = field.get(object);
        if (obj == null){
            SlothLog log = new SlothLog(object);
            log.error(MessageUtils.checkIfSingletonIsDefined(field));
        }
        singletonMap.put(singletonId, obj);
    }


    private void restartFirstActivity()
    {
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName() );

        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
        startActivity(i);
    }

    private boolean isBasicTypeOrSpecialType(Class clazz){
        if (clazz.isPrimitive()){
            return true;
        }
        return ((clazz.equals(String.class)) || (clazz.equals(Byte.class))
            || (clazz.equals(Short.class)) || (clazz.equals(Integer.class))
            || (clazz.equals(Long.class)) || (clazz.equals(Float.class))
            || (clazz.equals(Double.class)) || (clazz.equals(Boolean.class))
            || (clazz.equals(Character.class)));

    }

    public LayoutInflater getLayoutInflater(){
        return LayoutInflater.from(this);
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        log("onLowMemory");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        log("on terminate");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        log(newConfig.toString());
    }

    private void log(String message){
        Log.d("org.chil.mobile", message);
    }
}
