package es.slothdevelopers.slothframework.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.lang.reflect.Field;

import es.slothdevelopers.slothframework.SlothComponent;
import es.slothdevelopers.slothframework.injectors.SaveStateInjector;
import es.slothdevelopers.slothframework.injectors.SingletonInjector;
import es.slothdevelopers.slothframework.injectors.ViewInjector;
import es.slothdevelopers.slothframework.utils.SlothLog;

public class SlothActivity extends Activity implements SlothComponent {
    protected SlothLog log = new SlothLog(this);
    ViewInjector viewInjector = new ViewInjector(this);
    SingletonInjector singletonInjector = new SingletonInjector(this);
    SaveStateInjector saveStateInjector = new SaveStateInjector(this);

    public SlothActivity(){
        parseFields();
    }

    private void parseFields() {
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            singletonInjector.checkIfFieldIsSingleton(field);
            saveStateInjector.checkIfFieldToSaveState(field);
            viewInjector.checkIfViewToInject(field);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            saveStateInjector.restoreFields(savedInstanceState, "");
        }
        singletonInjector.processSingletons(getApplicationContext());
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        viewInjector.injectViewList(this);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        saveStateInjector.storeFields(savedInstanceState);
        super.onSaveInstanceState(savedInstanceState);
    }



    public void startActivityForClass(Class clazz){
        Intent intent = new Intent(getApplicationContext(), clazz);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        log.debug("Destroying sloth action bar activity");
    }

    @Override
    public Activity getSlothActivity() {
        return this;
    }
}
