package es.slothdevelopers.slothframework.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import es.slothdevelopers.slothframework.utils.SlothLog;

/**
 * Use with SlothViewHolder
 * @param <T>
 */
public class SlothRecyclerViewAdapter<T, W extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<W> {
    protected Context mContext;
    protected SlothLog log;
    protected List<T> data = null;

    public SlothRecyclerViewAdapter(Context context, List<T> data) {
        super();
        log = new SlothLog(this);

        this.mContext = context;
        this.data = data;
    }

    @Override
    public W onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(W holder, int position) {

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
