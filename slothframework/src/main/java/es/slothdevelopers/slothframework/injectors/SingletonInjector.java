package es.slothdevelopers.slothframework.injectors;

import android.content.Context;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

import es.slothdevelopers.slothframework.annotations.Singleton;
import es.slothdevelopers.slothframework.application.SlothApplication;
import es.slothdevelopers.slothframework.utils.MessageUtils;
import es.slothdevelopers.slothframework.utils.SlothLog;

import static es.slothdevelopers.slothframework.utils.MessageUtils.checkSlothApplicationMessage;

public class SingletonInjector {
    SlothLog log;
    private final Object caller;

    public SingletonInjector(Object caller) {
        this.log =new SlothLog(caller);
        this.caller = caller;
    }

    List<Field> singletonFieldList = new LinkedList<Field>();

    public void checkIfFieldIsSingleton(Field field) {
        if (field.isAnnotationPresent(Singleton.class)){
            if (field.getAnnotation(Singleton.class).value().equals("")){
                log.error(MessageUtils.checkSingletonParameter(field));
            }
            singletonFieldList.add(field);
        }
    }

    public void processSingletons(Context context) {
        if (singletonFieldList.isEmpty())
            return;

        SlothApplication slothApplication = getSlothContext(context);
        for (Field field: singletonFieldList){
            try {
                processSingleton(slothApplication, field);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private SlothApplication getSlothContext(Context context){
        try {
            SlothApplication slothApplication = (SlothApplication) context;
            return slothApplication;
        } catch (Exception e) {
            log.error(checkSlothApplicationMessage());
        }

        return null;
    }

    private void processSingleton(SlothApplication context, Field field) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        context.addSingletonField(caller, field);
    }
}
