package es.slothdevelopers.slothframework.exceptions;

/**
 * Created by jewi on 13/02/14.
 */
public class NotAnnotationValueException extends Exception{

    public NotAnnotationValueException(String detailMessage) {
        super(detailMessage);
    }
}
