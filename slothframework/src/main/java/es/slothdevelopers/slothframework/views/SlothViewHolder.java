package es.slothdevelopers.slothframework.views;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.slothdevelopers.slothframework.R;
import es.slothdevelopers.slothframework.injectors.InterfaceInjector;
import es.slothdevelopers.slothframework.injectors.SingletonInjector;
import es.slothdevelopers.slothframework.injectors.ViewInjector;
import es.slothdevelopers.slothframework.utils.SlothLog;

/**
 * Sloth Implementation of ViewHolder for to be used with SlothRecyclerViewAdapter
 */
public class SlothViewHolder extends RecyclerView.ViewHolder {
    ViewInjector viewInjector = new ViewInjector(this);
    SingletonInjector singletonInjector = new SingletonInjector(this);

    protected SlothLog log;

    public SlothViewHolder(View view) {
        super(view);
        log = new SlothLog(this);
        parseFields();
        viewInjector.injectViewList(view);
    }

    private void parseFields() {
        log.verbose("Parsing fields");
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            viewInjector.checkIfViewToInject(field);
            singletonInjector.checkIfFieldIsSingleton(field);
        }
    }
}
