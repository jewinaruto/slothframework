package es.slothdevelopers.slothframework.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;

import java.lang.reflect.Field;

import es.slothdevelopers.slothframework.SlothComponent;
import es.slothdevelopers.slothframework.activity.SlothActionBarActivity;
import es.slothdevelopers.slothframework.activity.SlothFragmentActivity;
import es.slothdevelopers.slothframework.injectors.InterfaceInjector;
import es.slothdevelopers.slothframework.injectors.SaveStateInjector;
import es.slothdevelopers.slothframework.injectors.SingletonInjector;
import es.slothdevelopers.slothframework.injectors.ViewInjector;
import es.slothdevelopers.slothframework.utils.MessageUtils;
import es.slothdevelopers.slothframework.utils.SlothLog;

public class SlothDialogFragment extends DialogFragment implements DialogInterface.OnCancelListener, SlothComponent {
    protected SlothLog log = new SlothLog(this);

    ViewInjector viewInjector = new ViewInjector(this);
    InterfaceInjector interfaceInjector = new InterfaceInjector(this);
    SingletonInjector singletonInjector = new SingletonInjector(this);
    SaveStateInjector saveStateInjector = new SaveStateInjector(this);

    public SlothDialogFragment(){
        parseFields();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        interfaceInjector.attachInterfacesToActivity(getActivity());
        interfaceInjector.attachInterfacesToParentFragment(getParentFragment());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            saveStateInjector.restoreFields(savedInstanceState, "");
        }
        singletonInjector.processSingletons(getActivity().getApplicationContext());
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK &&
                        event.getAction() == KeyEvent.ACTION_UP &&
                        !event.isCanceled()) {
                    return onBackPressed();
                }
                return false;
            }
        });
        return dialog;
    }

    private void parseFields() {
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            interfaceInjector.checkIfFieldIsInterfaceToBeAttached(field);
            singletonInjector.checkIfFieldIsSingleton(field);
            saveStateInjector.checkIfFieldToSaveState(field);
            viewInjector.checkIfViewToInject(field);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewInjector.injectViewList(getView());
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        saveStateInjector.storeFields(savedInstanceState);
        super.onSaveInstanceState(savedInstanceState);
    }

    public boolean onBackPressed() {
        return false;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            SlothFragmentActivity fa = (SlothFragmentActivity) getActivity();
            fa.unsubscribe(this);
        } catch (ClassCastException e) {
            try {
                SlothActionBarActivity fa = (SlothActionBarActivity) getActivity();
                fa.unsubscribe(this);
            } catch (ClassCastException cce){
                log.error(MessageUtils.checkParentIsSloth(getActivity().getClass().getSimpleName()));
            }
        }
    }

    @Override
    public Activity getSlothActivity() {
        return getActivity();
    }

}
